package io.linux.exceptions;

public class IncompleteCommand extends Exception {
	private static final long serialVersionUID = -2892605461099178633L;

	public IncompleteCommand(String msg) {
		super(msg);
	}
}