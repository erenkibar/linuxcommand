package io.linux.exceptions;

public class CannotCreateDirectory extends Exception {
	private static final long serialVersionUID = 5042961759957780997L;

	public CannotCreateDirectory(String msg) {
		super(msg);
	}
}
