package io.linux.main;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

import io.linux.constants.Commands;
import io.linux.exceptions.CannotCreateDirectory;
import io.linux.exceptions.CannotDeleteDirectory;
import io.linux.exceptions.DirectoryAlreadyExists;
import io.linux.exceptions.IncompleteCommand;
import io.linux.exceptions.InvalidDirectory;
import io.linux.exceptions.InvalidPath;
import io.linux.exceptions.UnrecognizedInput;
import io.linux.pojo.Command;
import io.linux.pojo.Directory;


public class Application {
	private static Directory rootDir; // Creating Root directory
	private static Stack<Directory> path; // Used to track present working directory
	private static Scanner sc;
	
	public Application() {
		rootDir = new Directory("/");
		path = new Stack<>();
	}
	
	/*
	 * This method does the job of list command
	 * */	
	private static void lsCommand() {
		Directory currDir;
		currDir = path.peek();
		if(!currDir.getAllDirectories().isEmpty()) {
			System.out.print("DIRS:");
			for (String name : currDir.getAllDirectories()) {
				System.out.print(" " + name);
			}
			System.out.println();
		}else {
			System.out.println("INFO: DIRECTORY DOES NOT CONTAIN ANY DIRECTORIES.");
		}
	}
	
	/*
	 * This method does the job of make directory command
	 * */	
	private static void mkdirCommand(String[] line) throws DirectoryAlreadyExists, InvalidPath, CannotCreateDirectory, IncompleteCommand {
		Directory tmpDir;
		if(line.length == 1) {
			throw new IncompleteCommand("PROVIDE SOME PATH");
		} 
		else {
			for(int j = 1; j < line.length; ++j) {
				String pathToDir = line[j];
				String dirs[] = pathToDir.split("/");
				int start = 0;
				
				if(dirs.length == 0) {
					throw new CannotCreateDirectory("CREATION OF ANOTHER ROOT DIRECTORY IS NOT ALLOWED");
				}
				if(dirs[0].equals("/")) { // absolute path
					tmpDir = rootDir;
					start = 1;
				} else { // relative path
					tmpDir = path.peek();
					if(dirs[0].equals(".")) start = 1;
				}
				
				int i;
				for(i = start; i < dirs.length-1; ++i) {
					if(!tmpDir.isPresent(dirs[i])) {
						throw new InvalidPath("INVALID PATH.");
					}
					tmpDir = tmpDir.getDirectory(dirs[i]);
				}
				if(tmpDir.isPresent(dirs[i])) {
					throw new DirectoryAlreadyExists(dirs[i] + " DIRECTORY ALREADY EXISTS.");
				}
				tmpDir.add(new Directory(dirs[i]));
				System.out.println("SUCC: " + dirs[i] + " CREATED.");
			}
		}
	}
	
	/*
	 * This method does the job of change directory command
	 * */	
	private static void cdCommand(String[] line) throws IncompleteCommand, InvalidPath {
		Directory tmpDir;
		if(line.length == 1) {
			throw new IncompleteCommand("PROVIDE SOME PATH");
		} 
		else {
			String pathToDir = line[1];
			String dirs[] = pathToDir.split("/");
			//Path shouldnt be null
			//path = null;
			int start = 0;
			
			if(dirs.length == 0) { // when path is "/" i.e. root directory

				while(!path.peek().getName().equals("/")) {
					path.pop();
				}
			} else {
				if(dirs[0].equals("")) { // absolute path
					tmpDir = rootDir;
					start = 0;
					while(!path.peek().getName().equals("/")) {
						path.pop();
					}
					
				} else { // relative path
					tmpDir = path.peek();
					if(dirs[0].equals(".")) start = 1;
				}
				
				for(int i = start; i < dirs.length; ++i) {
					if(!tmpDir.isPresent(dirs[i])) {
						throw new InvalidPath("INVALID PATH.");
					}
					tmpDir = tmpDir.getDirectory(dirs[i]);
					path.push(tmpDir);
				}
			}
			System.out.println("SUCC: REACHED.");
		}
	}
	
	/*
	 * This method does the job of remove command
	 * */	
	private static void rmCommand(String[] line) throws InvalidPath, CannotDeleteDirectory, InvalidDirectory, IncompleteCommand {
		Directory tmpDir;
		if(line.length == 1) {
			throw new IncompleteCommand("PROVIDE SOME PATH");
		} 
		else {
			for(int j = 1; j < line.length; ++j) {
				String pathToDir = line[j];
				String dirs[] = pathToDir.split("/");
				int start = 0;
				
				if(dirs.length == 0) { // trying to delete root directory
					throw new CannotDeleteDirectory("CANNOT DELETE ROOT DIRECTORY");
				}
				
				if(dirs[0].equals("")) { // absolute path
					tmpDir = rootDir;
					start = 1;
					
					/*
					 * checking whether the directory to be deleted is direct/indirect parent of current working directory.
					 * */
					Map<String, Boolean> tmpMap = new HashMap<>();
					for(int i = 0; i < dirs.length; ++i) {
						tmpMap.put(dirs[i], true);
					}
					Stack<Directory> tmp = new Stack<>();
					boolean present = false;
					while(!path.isEmpty()) {
						Directory d = path.pop();
						tmp.push(d);
						if(tmpMap.containsKey(d.getName())) {
							present = true;
							break;
						}
					}
					while(!tmp.isEmpty()) {
						path.push(tmp.pop());
					}
					if(present) {
						throw new CannotDeleteDirectory("CANNOT DELETE DIRECTORY: EITHER IT IS CURRENT WORKING DIRECTORY OR DIRECT/INDIRECT PARENT OF THE "
								+ "CURRENT WORKING DIRECTORY. GO TO PARENT DIRECORY OF THE DIRECTORY YOU WANT TO DELETE AND TRY AGAIN.");
					}
				} else { // relative path
					tmpDir = path.peek();
					if(dirs[0].equals(".")) start = 1;
				}
				
				for(int i = start; i < dirs.length-1; ++i) {
					if(!tmpDir.isPresent(dirs[i])) {
						throw new InvalidPath("INVALID PATH.");
					}
					tmpDir = tmpDir.getDirectory(dirs[i]);
				}
				
				String dirToRemove = dirs[dirs.length-1];
				if(!tmpDir.isPresent(dirToRemove)) {
					throw new InvalidDirectory(dirToRemove + " DIRECTORY DOES NOT EXIST.");
				}
				tmpDir.remove(dirToRemove);
				System.out.println("SUCC: " + dirToRemove +" DELETED");
			}
		}
	}
	
	/*
	 * This method does the job of present working directory command
	 * */	
	private static void pwdCommand() {
		Stack<Directory> temp = new Stack<>();
		StringBuilder ans = new StringBuilder();
		Directory d;
		while(!path.isEmpty()) {
			temp.push(path.pop());
		}
		
		while(!temp.isEmpty()) {
			d = temp.pop();
			path.push(d);
			
			if(!d.getName().equals("/")) {
				ans.append("/" + d.getName());
			}
		}
		if(ans.length() == 0) ans.append("/");
		System.out.println("PATH: " + ans);
	}
	
	/*
	 * Main method starts
	 * */
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Application app = new Application();
		
		/*Creating list of valid commands
		 * Add more commands here and handle the operation
		 * in the If-else condition below
		 * */
		Command command = new Command();
		command.add("cd");
		command.add("pwd");
		command.add("ls");
		command.add("mkdir");
		command.add("rm");

		path.push(rootDir); // initializing the pwd to root dir
		
		String cmd, line[];
		sc = new Scanner(System.in);
		System.out.println("Starting application...");
		while(true) {
			System.out.print("$>");
			try{
				line = sc.nextLine().split(" ");
				cmd = line[0];
				
				if(command.contains(cmd)) {
					if(cmd.equals(Commands.CD.toLowerCase())) { // change directory command
						Application.cdCommand(line);
					}
					else if(cmd.equals(Commands.MKDIR.toLowerCase())) { // create directory command
						Application.mkdirCommand(line);
					}
					else if(cmd.equals(Commands.LS.toLowerCase())) { // list all directories within current directory
						Application.lsCommand();
					}
					else if(cmd.equals(Commands.RM.toLowerCase())) { // remove specified directory
						Application.rmCommand(line);
					}
					else if(cmd.equals(Commands.PWD.toLowerCase())) { // present working directory
						Application.pwdCommand();
					}
				}else{
					if(line.length == 2 && line[0].equals("session") && line[1].equals("clear")) { // reset application (special command)
						rootDir.setMap(new HashMap<>());
						path.clear();
						path.push(rootDir);
						System.out.println("SUCC: CLEARED: RESET TO ROOT.");
					}else {
						throw new UnrecognizedInput("CANNOT RECOGNIZE INPUT.");
					}
				}
			}
			catch(UnrecognizedInput ui) {
				System.out.println("ERR: " + ui.getMessage());
			} catch(InvalidPath ip) {
				System.out.println("ERR: " + ip.getMessage());
			} catch(DirectoryAlreadyExists dae) {
				System.out.println("ERR: " + dae.getMessage());
			} catch(IncompleteCommand ic){
				System.out.println("INFO: " + ic.getMessage());
			} catch(InvalidDirectory id){
				System.out.println("ERR: " + id.getMessage());
			} catch(CannotDeleteDirectory cdf){
				System.out.println("ERR: " + cdf.getMessage());
			} catch(CannotCreateDirectory ccd){
				System.out.println("ERR: " + ccd.getMessage());
			} catch(Exception e) {
				System.out.println(e);
			}
		}
	}
}
