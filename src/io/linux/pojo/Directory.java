package io.linux.pojo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Directory {
	String name;
	Map<String, Directory> map;
	Map<String, Directory> maps;
	
	public Directory(){}
	public Directory(String path) {
		this.map = new HashMap<>();
		this.name = path;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Map<String, Directory> getMap() {
		return map;
	}
	public void setMap(Map<String, Directory> map) {
		this.map = map;
	}
	
	public void add(Directory dir) {
		this.map.put(dir.name, dir);
	}
	public void remove(String dirName) {
		this.map.remove(dirName);
	}
	//Change return maps to map.
	public Directory getDirectory(String dirName) {
		return this.map.get(dirName);
	}
	
	public Set<String> getAllDirectories() {
		return this.map.keySet();
	}
	//Fixing isPresent for mkdir already exists.
	public boolean isPresent(String dirName) { return this.map.containsKey(dirName);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Directory other = (Directory) obj;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}