package io.linux.constants;

public class Commands {
	public static final String LS = "LS";
	public static final String MKDIR = "MKDIR";
	public static final String PWD = "PWD";
	public static final String CD = "CD";
	public static final String RM = "RM"; 
}