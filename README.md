****A java console application that imitates some of the linux commands logically****

	1. ls : This command lists all the directories/files present in the current working directory.

	2. pwd : It prints the present working directory.

	3. mkdir : It creates a new directory in the provided path.

	Syntax: mkdir dirName
	
	4. rm : It deletes the mentioned directory/file.
	Syntax: rm dirName
	
	5. cd : It allows user to go to any mentioned directory.